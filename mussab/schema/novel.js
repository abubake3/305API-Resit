//sets up the database for novels
var mongoose = require('mongoose');

var NovelSchema   = new mongoose.Schema({
  title: String,
  writer: String,
  isbn: Number
});


module.exports = mongoose.model('Novel', NovelSchema);