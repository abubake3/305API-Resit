//this files sets up the database for users and adds security to keep passwords secure 
var mongoose = require('mongoose');

var bcrypt = require('bcrypt-nodejs');


//sets up username and password for the database
var UserSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});


//adds security by hashing passwords
UserSchema.pre('save', function(callback) {
    var user = this;
   
    if(!user.isModified('password')) return callback();
   
    bcrypt.genSalt(5, function(err, salt) {
        if(err) return callback(err);
        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if(err) return callback(err);
            user.password = hash;
            callback();
        });
    });
});


UserSchema.methods.verifyPassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if(err) return cb(err);
        cb(null, isMatch);
    });
};


//sets up the document for use in other files
module.exports = mongoose.model('User', UserSchema);