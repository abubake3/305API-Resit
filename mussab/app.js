
//theese are the modules needed to run the api
var express = require('express');
var passport = require('passport');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var Novel = require('./schema/novel');
var User = require('./schema/user');
var request = require('request');

//connects to the database
mongoose.connect('mongodb://localhost:27017/novels');

var app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));


app.use(passport.initialize());
// Create our Express router
var router = express.Router();





//displays the message no novels here at the / endpoint
router.get('/', function(req, res) {
  res.json({ message: 'no novels here' }); 
});

// sets the endpoint to post novels
var novelsRoute = router.route('/novels');

//code for posting novels
novelsRoute.post( function(req, res) {
  
  var novel = new Novel();

 
  novel.title = req.body.title;
  novel.author = req.body.author;
  novel.isbn = req.body.isbn;

  
  novel.save(function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'novel added', data: novel });
  });
});


//code for reading novels
novelsRoute.get( function(req, res) {
  
  Novel.find(function(err, novels) {
    if (err)
      res.send(err);

    res.json(novels);
  });
});


// sets the /novels/:novel_id endpoint 
var novelRoute = router.route('/novels/:novel_id');

//code to view novels by id 
novelRoute.get( function(req, res) {
  
  Novel.findById(req.params.novel_id, function(err, novel) {
    if (err)
      res.send(err);

    res.json(novel);
  });
});

//code to update novels
novelRoute.put( function(req, res) {
  
  Novel.findById(req.params.novel_id, function(err, novel) {
    if (err)
      res.send(err);

    novel.isbn = req.body.isbn;

    
    novel.save(function(err) {
      if (err)
        res.send(err);

      res.json(novel);
    });
  });
});

//code to delete the novels
novelRoute.delete( function(req, res) {
  
  Novel.findByIdAndRemove(req.params.novel_id, function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'novel removed' });
  });
});





//sets the /users endpoint
var usersRoute = router.route('/users');

//code to post the users
usersRoute.post(function(req, res) {
    var user = new User({
        username: req.body.username,
        password: req.body.password
    });
    user.save(function(err) {
        if(err) res.send(err);
        res.json({
            message: 'new user'
        });
    });
});

//code to view the users
usersRoute.get( function(req, res) {
    User.find(function(err, users) {
        if(err) res.send(err);
        res.json(users);
    });
});


//sets the endpoing for /user_id
var userRoute = router.route('/users:user_id');


//code to delete users
 userRoute.delete( function(req, res) {
   
    User.remove({
        _id: req.params.user_id,
    }, function(err) {
        if(err) res.send(err);
        res.json({
            message: 'removed '
        });
    });
});


//uses request module to find books by isbn
app.post('/api/search', function(req, res) {
    req.body.search
    request('https://www.googleapis.com/books/v1/volumes?q=' + req.body.search + '&maxResults=1', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body)
            res.send(body)
        }
})
});


//code responsible for mapping the endpoints
app.use('/api', router);
// Start the server
app.listen(3000);
